export namespace NsUser {

    export interface InterfaceUser {
        id: number;
        name: string;
        email: string;
        gender: string;
        status: string;
        created_at: string;
        updated_at: string;
    }

    export interface InterfaceGetRpta {
        code: number;
        meta: {
            pagination: {
                total: number;
                pages: number;
                page: number;
                limit: number;
            }
        };
        data: InterfaceUser[];
    }

    export interface InterfaceNew {
        name: string;
        email: string;
        gender: string;
        status: string;       
    }

    export interface InterfaceEdit {
        name: string;
        email: string;
        status: string;       
    }
   
}
