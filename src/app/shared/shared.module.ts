import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ComboCatalogoComponent } from './combo-catalogo/combo-catalogo.component';

@NgModule({
    declarations: [
        FooterComponent,
        HeaderComponent,
        ComboCatalogoComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        FooterComponent,
        HeaderComponent,
        ComboCatalogoComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: []
})
export class SharedModule { }