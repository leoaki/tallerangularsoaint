import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboCatalogoComponent } from './combo-catalogo.component';

describe('ComboCatalogoComponent', () => {
  let component: ComboCatalogoComponent;
  let fixture: ComponentFixture<ComboCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
