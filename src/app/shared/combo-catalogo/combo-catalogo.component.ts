import { Component, EventEmitter, Input,Output } from '@angular/core';
import { InterfaceCatalog } from '@taller/interfaces';

@Component({
  selector: 'app-combo-catalogo',
  templateUrl: './combo-catalogo.component.html',
  styleUrls: ['./combo-catalogo.component.css'],
})
export class ComboCatalogoComponent {

  @Input() listado: InterfaceCatalog[];
  @Input() onlyRead: boolean = false; // Flag para definir si el vombo es de solo lectura  | deLectura
  @Output() eventChange: EventEmitter<number> = new EventEmitter(); // Comportamiento change
  constructor() { }

}