export class ConstantUris {

    public static readonly USERS = 'users';
    public static readonly POST = 'posts';
    public static readonly COMMENT = 'comments';
}
