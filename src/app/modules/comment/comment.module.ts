import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCommentComponent } from './components/list-comment/list-comment.component';
import { EditCommentComponent } from './components/edit-comment/edit-comment.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { CommentRoutingModule } from './comment-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ListCommentComponent, EditCommentComponent, AddCommentComponent
  ],
  imports: [
    CommonModule,
    CommentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CommentModule { }
