import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { EditCommentComponent } from './components/edit-comment/edit-comment.component';
import { ListCommentComponent } from './components/list-comment/list-comment.component';

const routes: Routes = [
    { path: '', component: ListCommentComponent},
    { path: 'listar', component: ListCommentComponent},
    { path: 'agregar', component: AddCommentComponent},
    { path: 'editar/:id', component: EditCommentComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommentRoutingModule {}