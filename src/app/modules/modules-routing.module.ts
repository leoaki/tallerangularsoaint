import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModulesComponent } from './modules.component';

const routes: Routes = [
    { path: '', component: ModulesComponent,
      children: [
        { path: '', redirectTo: 'usuarios', pathMatch: 'full'},
        { path: 'usuarios',
          loadChildren: () => import('./user/user.module').then(m => m.UserModule)
        },
        { path: 'post',
          loadChildren: () => import('./post/post.module').then(m => m.PostModule)
        },
        {path: 'comentarios',
         loadChildren: () => import('./comment/comment.module').then(m => m.CommentModule)}
      ]}
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ModulesRoutingModule { }