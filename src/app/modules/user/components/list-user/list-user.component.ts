import { Component, OnInit, ViewChild } from '@angular/core';
import { InterfaceCatalog } from '@taller/interfaces';
import * as data from '../../../../data/sexo.json';
import { UserService } from '../../services/user.service';
import { HijovcComponent } from '../hijovc/hijovc.component';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  listado: InterfaceCatalog[] = [];
  @ViewChild(HijovcComponent, {static: false}) hijovc: HijovcComponent;
  edad: number;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.listado = (data as any).default;
  }

  getNumber(): number {
    console.log('this.hijovc.getNumberRandom()=>', this.hijovc.getNumberRandom());
    return this.hijovc.getNumberRandom();
  }

  getEnable(): void {
    alert('El usuario está ' + (this.hijovc.isEnabled() == true? 'autorizado': 'prohibida su venta'));
  }

}
