import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hijovc',
  templateUrl: './hijovc.component.html',
  styleUrls: ['./hijovc.component.css']
})
export class HijovcComponent implements OnInit {

  @Input() name: string;
  @Input() edad: number;
  constructor() { }

  ngOnInit() {
  }

  getNumberRandom(): number {
    return Math.random();
  }

  isEnabled(): boolean {
    return this.edad >= 18? true: false;
  }

}
