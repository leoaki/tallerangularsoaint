import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HijovcComponent } from './hijovc.component';

describe('HijovcComponent', () => {
  let component: HijovcComponent;
  let fixture: ComponentFixture<HijovcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HijovcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HijovcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
