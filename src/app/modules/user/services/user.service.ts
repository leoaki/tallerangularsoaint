import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantUris } from '@taller/constants';
import { NsUser } from '@taller/interfaces';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<NsUser.InterfaceGetRpta> {
    return this.http.get<NsUser.InterfaceGetRpta>(environment.URI_BASE + ConstantUris.USERS);
  }

  update(user: NsUser.InterfaceEdit): Observable<NsUser.InterfaceGetRpta> {
    return this.http.patch<NsUser.InterfaceGetRpta>(environment.URI_BASE + ConstantUris.USERS, user);
  }

  new(user: NsUser.InterfaceNew): Observable<NsUser.InterfaceGetRpta> {
    return this.http.post<NsUser.InterfaceGetRpta>(environment.URI_BASE + ConstantUris.USERS, user);
  }

  delete(idUser: number) {
    return this.http.delete(environment.URI_BASE + ConstantUris.USERS + '/' + idUser);
  }
  
}
