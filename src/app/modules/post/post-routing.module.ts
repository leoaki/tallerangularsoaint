import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPostComponent } from './components/add-post/add-post.component';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { ListPostComponent } from './components/list-post/list-post.component';

const routes: Routes = [
    { path: '', component: ListPostComponent},
    { path: 'listar', component: ListPostComponent},
    { path: 'agregar', component: AddPostComponent},
    { path: 'editar/:id', component: EditPostComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PostRoutingModule {}