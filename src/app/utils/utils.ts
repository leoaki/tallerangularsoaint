export function trim(value: any): string {
    if (isEmpty(value)) { return ''; }
    return value.toString().trim();
}

export function isEmpty(value: any): boolean {
    if (value == null || value === undefined) { return true; }

    if (value.__proto__.constructor === String) {
        return value.trim().length === 0;
    }
    if (value.__proto__.constructor === Array) {
        return value.length === 0;
    }
    if (value.__proto__.constructor === Object) {
        return Object.getOwnPropertyNames(value).length === 0;
    }
    return false;
}